import { Component, OnInit } from '@angular/core';
import { ChatserviceService } from 'src/app/services/chatservice.service';
import { TokenserviceService } from '../../services/tokenservice.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  constructor(private tokenservice: TokenserviceService, private chatservice: ChatserviceService) {}

  ngOnInit(): void {}
  logout() {
    this.chatservice.userDisconnect();
    this.tokenservice.logout();
    //window.sessionStorage.clear();
    window.location.reload();
  }
}
