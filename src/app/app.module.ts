import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { HeaderComponent } from './shared/header/header.component';
import { HomeComponent } from './pages/home/home.component';
import { ChartComponent } from './pages/chart/chart.component';
import { TableComponent } from './pages/table/table.component';
import { MapComponent } from './pages/map/map.component';
import { InformationComponent } from './pages/information/information.component';
import { WeatherComponent } from './pages/weather/weather.component';
import { HttpClientModule } from '@angular/common/http';
import { CovidComponent } from './pages/covid/covid.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { ChatComponent } from './pages/chat/chat.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ValidConfirm } from './services/validconfirm';
import { AuthenGuard } from './services/authenguard';

@NgModule({
  declarations: [
    AppComponent,
    ValidConfirm,
    SidebarComponent,
    HeaderComponent,
    HomeComponent,
    ChartComponent,
    TableComponent,
    MapComponent,
    InformationComponent,
    WeatherComponent,
    CovidComponent,
    ChatComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, NgApexchartsModule, FormsModule, ReactiveFormsModule],
  providers: [AuthenGuard],
  bootstrap: [AppComponent]
})
export class AppModule {}
