import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CovidserviceService {
  constructor(private http: HttpClient) {}
  getCovid() {
    return this.http.get('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
  }
  getCovid_data() {
    return this.http.get('https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-all');
  }
}
