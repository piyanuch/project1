import { JsonpClientBackend } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { windowWhen } from 'rxjs';

const tokenkey = 'auth-token';
const userkey = 'auth-user';

@Injectable({
  providedIn: 'root'
})
export class TokenserviceService {
  constructor() {}
  // function logout = clear
  logout() {
    window.sessionStorage.clear();
  }
  // call new token replace old token
  public savetoken(token: string) {
    window.sessionStorage.removeItem(tokenkey);
    window.sessionStorage.setItem(tokenkey, token);
  }
  // return tokenkey => function
  public gettoken() {
    return sessionStorage.getItem(tokenkey);
  }
  // remove old user and keep new user - json
  public saveuser(user) {
    window.sessionStorage.removeItem(userkey);
    //window.sessionStorage.setItem(userkey, user); -> get json
    window.sessionStorage.setItem(userkey, JSON.stringify(user)); // json -> string
  }
  // retrun userkey => function
  public getuser() {
    // return sessionStorage.getItem(userkey); -> get string
    return JSON.parse(sessionStorage.getItem(userkey)); // get json
  }
}
