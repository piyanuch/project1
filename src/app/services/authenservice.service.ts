import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TokenserviceService } from './tokenservice.service';

// authen ไว้รับส่งข้อมูลขึ้น server
const auth_api = 'http://localhost:4500/api/auth/'; // set path
const httpoption = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

@Injectable({
  providedIn: 'root'
})
export class AuthenserviceService {
  constructor(private http: HttpClient, private tokenservice: TokenserviceService) {}
  login(data): Observable<any> {
    return this.http.post(auth_api + 'login', { email: data.email, password: data.password }, httpoption);
  }
  register(data): Observable<any> {
    return this.http.post(
      auth_api + 'register',
      {
        firstname: data.firstname,
        lastname: data.lastname,
        email: data.email,
        password: data.password,
        displayname: data.displayname
      },
      httpoption
    );
  }
  public checklogin(): boolean {
    if (this.tokenservice.gettoken()) {
      return true;
    } else {
      return false;
    }
  }
}
