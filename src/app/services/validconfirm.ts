import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validators } from '@angular/forms';

@Directive({
  selector: '[appValidConfirm]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ValidConfirm,
      multi: true
    }
  ]
})
export class ValidConfirm implements Validators {
  @Input() appValidConfirm: string;
  validate(control: AbstractControl): { [key: string]: any } | null {
    const controlToCompare = control.parent.get(this.appValidConfirm);
    if (controlToCompare && controlToCompare.value !== control.value) {
      return { notEqual: true };
    }
    return null;
  }
}
