import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChartComponent } from './pages/chart/chart.component';
import { ChatComponent } from './pages/chat/chat.component';
import { CovidComponent } from './pages/covid/covid.component';
import { HomeComponent } from './pages/home/home.component';
import { InformationComponent } from './pages/information/information.component';
import { LoginComponent } from './pages/login/login.component';
import { MapComponent } from './pages/map/map.component';
import { RegisterComponent } from './pages/register/register.component';
import { TableComponent } from './pages/table/table.component';
import { WeatherComponent } from './pages/weather/weather.component';
import { AuthenGuard } from './services/authenguard';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'weather',
    canActivate: [AuthenGuard],
    component: WeatherComponent
  },
  {
    path: 'home',
    canActivate: [AuthenGuard],
    component: HomeComponent
  },
  {
    path: 'map',
    canActivate: [AuthenGuard],
    component: MapComponent
  },
  {
    path: 'chart',
    canActivate: [AuthenGuard],
    component: ChartComponent
  },
  {
    path: 'table',
    canActivate: [AuthenGuard],
    component: TableComponent
  },
  {
    path: 'information',
    canActivate: [AuthenGuard],
    component: InformationComponent
  },
  {
    path: 'covid',
    canActivate: [AuthenGuard],
    component: CovidComponent
  },
  {
    path: 'chat',
    canActivate: [AuthenGuard],
    component: ChatComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
