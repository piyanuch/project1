import { Component, OnInit, ViewChild } from '@angular/core';
import { CovidserviceService } from '../../services/covidservice.service';
import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexFill,
  ApexYAxis,
  ApexTooltip,
  ApexTitleSubtitle,
  ApexPlotOptions,
  ApexXAxis,
  ApexNonAxisChartSeries,
  ApexResponsive
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  yaxis: ApexYAxis | ApexYAxis[];
  title: ApexTitleSubtitle;
  labels: string[];
  stroke: any; // ApexStroke;
  dataLabels: any; // ApexDataLabels;
  fill: ApexFill;
  plotOptions: ApexPlotOptions;
};

export type ChartPieOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
};
/*import {
  Chart,
  LineController,
  LineElement,
  PointElement,
  LinearScale,
  Title,
  BarController,
  BarElement,
  CategoryScale
} from 'node_modules/chart.js'; */

/*import * as Chart from 'node_modules/chart.js'; */

@Component({
  selector: 'app-covid',
  templateUrl: './covid.component.html',
  styleUrls: ['./covid.component.scss']
})
export class CovidComponent implements OnInit {
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartPieOptions: Partial<ChartPieOptions>;
  covid: any = [];
  coviddata: any = [];
  death: any = [];
  recovery: any = [];
  case: any = [];
  daily: any = [];

  constructor(private covidService: CovidserviceService) {
    /* Chart.register(
      BarController,
      BarElement,
      LineController,
      LineElement,
      PointElement,
      LinearScale,
      Title,
      CategoryScale 
    ); */
  }

  ngOnInit(): void {
    //pie chart
    this.covidService.getCovid().subscribe((response: any) => {
      this.covid = response;

      this.chartPieOptions = {
        series: [this.covid[0].new_case, this.covid[0].total_death, this.covid[0].new_recovered],
        chart: {
          width: 440,
          type: 'pie'
        },
        labels: ['NEW CASE', 'TOTAL DEATH', 'NEW RECOVERED'],
        responsive: [
          {
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }
        ]
      };
    });
    // Graph
    this.covidService.getCovid_data().subscribe((response: any) => {
      this.coviddata = response;
      // for loop get data
      for (let i = 0; i < this.coviddata.length; i++) {
        this.case[i] = this.coviddata[i].total_case;
        this.recovery[i] = this.coviddata[i].total_recovered;
        this.death[i] = this.coviddata[i].total_death;
        this.daily[i] = this.coviddata[i].txn_date;
      }
      // for loop draw

      //line chart
      this.chartOptions = {
        series: [
          {
            name: 'TOTAL CASE',
            type: 'column',
            data: this.case
          },
          {
            name: 'TOTAL DEATH',
            type: 'column',
            data: this.death
          },
          {
            name: 'TOTAL RECOVERED',
            type: 'line',
            data: this.recovery
          }
        ],
        chart: {
          height: 280,
          type: 'line',
          stacked: true
        },
        stroke: {
          width: 3
        },
        title: {
          text: 'COVID-19'
        },
        labels: this.daily,
        xaxis: {
          type: 'datetime'
        }
      };
    });
  }
}
