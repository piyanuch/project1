import { Component, OnInit } from '@angular/core';
import { AuthenserviceService } from '../../services/authenservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  constructor(private authen: AuthenserviceService, private router: Router) {}
  form: any = {};
  SignUpFailed = false;
  Successful = false;
  errorMessage = '';

  ngOnInit(): void {}
  onSubmit() {
    this.authen.register(this.form).subscribe(
      (data) => {
        this.SignUpFailed = false;
        this.Successful = true;
        this.reDirect();
      },
      (error) => {
        this.errorMessage = error.error.message;
        this.SignUpFailed = true;
      }
    );
  }
  reDirect() {
    this.router.navigate(['/login']); // login success go to link
  }
}
