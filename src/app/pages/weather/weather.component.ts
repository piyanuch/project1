import { Component, OnInit } from '@angular/core';
import { WeatherserviceService } from 'src/app/services/weatherservice.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  weather: any = [];
  temp_c: number = 0;
  constructor(private weatherService: WeatherserviceService) {}

  ngOnInit(): void {
    this.weatherService.getWeather().subscribe((response: any) => {
      this.weather = response;
      console.log(this.weather);
      this.temp_c = Math.round(this.weather.main.temp - 273.15);
    });
  }
}
