import { Component, OnInit } from '@angular/core';
import { TokenserviceService } from '../../services/tokenservice.service';
import { AuthenserviceService } from '../../services/authenservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  LoginFailed = false;
  LoggedIn = false;
  form: any = {};
  errorMessage = '';
  constructor(private token: TokenserviceService, private authen: AuthenserviceService, private router: Router) {
    // import lib
  }

  ngOnInit(): void {
    if (this.token.gettoken()) {
      // recheck login ? เริ่มต้นด้วย false
      this.LoggedIn = true;
    } else {
      this.LoggedIn = false;
    }
  }
  onSubmit() {
    this.authen.login(this.form).subscribe(
      (data) => {
        //หลังจากกด submit
        this.token.savetoken(data.accessToken);
        this.token.saveuser(data);
        this.LoginFailed = false;
        this.LoggedIn = true;
        this.reDirect();
      },
      (error) => {
        this.errorMessage = error.error.message;
        this.LoginFailed = true;
      }
    );
  }
  reDirect() {
    this.router.navigate(['/home']); // login success go to link
  }
}
