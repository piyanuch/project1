import { Component, OnInit } from '@angular/core';
import { CovidserviceService } from '../../services/covidservice.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  covid: any = [];

  constructor(private covidService: CovidserviceService) {}

  ngOnInit(): void {
    this.covidService.getCovid().subscribe((response: any) => {
      this.covid = response;
    });
  }
}
