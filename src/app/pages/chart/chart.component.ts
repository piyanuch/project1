import { ParsedVariable } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import {
  Chart,
  LineController,
  LineElement,
  PointElement,
  LinearScale,
  Title,
  BarController,
  BarElement,
  CategoryScale
} from 'node_modules/chart.js';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  constructor() {
    Chart.register(
      BarController,
      BarElement,
      LineController,
      LineElement,
      PointElement,
      LinearScale,
      Title,
      CategoryScale
    );
  }

  ngOnInit() {
    const labels = ['January', 'February', 'March', 'April', 'May', 'June'];
    const data = {
      labels: labels,
      datasets: [
        {
          label: 'My First dataset',
          data: [1, 10, 5, 2, 20, 30, 45],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderWidth: 1,
          borderColor: '#58111A',
          maxBarThickness: 400
        }
      ]
    };
    let myChart = new Chart('myChart', {
      type: 'bar',
      data: data,
      options: {}
    });
  }
}
